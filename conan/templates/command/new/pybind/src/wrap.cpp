#include <pybind11/pybind11.h>

int add(int i, int j) {
    return i + j;
}

PYBIND11_MODULE({{name}}, m) {
    m.doc() = "{{name}} example plugin"; // optional module docstring

    m.def("add", &add, "A function which adds two numbers");
}
