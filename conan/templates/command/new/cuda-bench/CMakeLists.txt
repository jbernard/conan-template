cmake_minimum_required(VERSION 3.18)

project({{name}} VERSION {{version}} LANGUAGES CXX CUDA)

add_executable({{name}} src/main.cu)
target_include_directories({{name}} PUBLIC include)

target_compile_features({{name}} PUBLIC cxx_std_17)

find_package(CUDAToolkit REQUIRED)
find_package(benchmark REQUIRED)

target_link_libraries({{name}} PUBLIC
    CUDA::cudart
    benchmark::benchmark
)
