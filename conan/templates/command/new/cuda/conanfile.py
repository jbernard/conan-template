from conans import ConanFile
from conan.tools.cmake import CMake

class {{name}}Conan(ConanFile):
    name = '{{name}}'
    version = '{{version}}'
    license = ''

    settings = 'os', 'compiler', 'build_type', 'arch'
    generators = 'CMakeDeps', 'CMakeToolchain'

    requires = ['fmt/9.1.0']

    def build(self):
        cmake = CMake(self)

        if self.should_configure:
            cmake.configure()
        if self.should_build:
            cmake.build()
