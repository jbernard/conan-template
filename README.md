# Conan template

Conan template rely on `conan new` command and the possibility to provide preconfigured templates projects even if they do not rely on conan.

## Install

Clone into project :

```bash
git clone https://gitlab.obspm.fr/jbernard/conan-template.git
```

Then create symbolink link. The simpler way consist on using [GNU Stow](https://www.gnu.org/software/stow/).

```bash
cd conan-template
stow -S conan -t ~/.conan
```

## Usage

In an empty directory run the following command :

```bash
conan new <project_name>/<version> -m <template_name>
```

A project is initiated in your current directory.

## Example

Create en executable project using `CUDA`.

```bash
conan new cuda_test/0.1 -m cuda
```

The command will display the following output :

```bash
File saved: ./CMakeLists.txt
File saved: ./conanfile.txt
File saved: ./configure.sh
File saved: ./run.sh
File saved: src/main.cu
```

It list all the files that are created.

This template provide simple scripts to configure, build and run the executable.

```bash
bash configure.sh
bash run.sh
```

> **NOTE:**  `conan new` does not transfer file mode. You will need to make them executable or use `bash` command.
